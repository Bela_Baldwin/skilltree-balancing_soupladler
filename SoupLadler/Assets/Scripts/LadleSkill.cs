﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace LAPMTech
{
	public class LadleSkill : MonoBehaviour
	{
		[SerializeField]
		private LadleCreation [] ladleUpgrade;
		private string description;
		private float purchaseCost;
		private float costReductionMod;
		private int skillLevel;
		[SerializeField]
		private LadleCreation currentLadleInUse;
		private bool ladleNeedsUpgrade;

		[SerializeField]
		private GameObject [] activeHelpers;
		private string activeHelper = "Helper";
		[SerializeField]
		private float baseTimeCost = 10f;

		private CurrencyCreation currencyCreation;
		private GameObject ladleSkillButton;
		private string helperButton = "LadleSkillButton";
		[SerializeField]
		private float timer;
		private bool ladleButtonDisabled;

		[SerializeField]
		private TextMeshProUGUI buttonText;
		[SerializeField]
		private TextMeshProUGUI skillTipText;
		[SerializeField]
		private TextMeshProUGUI skillLevelText;

		// Updates relevant info for the button texts
		private void Awake()
		{
			description = currentLadleInUse.description;
			costReductionMod = currentLadleInUse.costReductionMod;
			purchaseCost = currentLadleInUse.purchaseCost;

			buttonText.text = currentLadleInUse.name;
			skillTipText.text = description + " [COST: " + purchaseCost + " ]";
			skillLevelText.text = skillLevel.ToString() + " / 5";
		}

		// Start is called before the first frame update
		// Gets the relevant references
		// Update relevant variables to most recent values
		void Start()
		{
			currencyCreation = FindObjectOfType<CurrencyCreation>();
			ladleSkillButton = GameObject.FindGameObjectWithTag(helperButton);

			description = currentLadleInUse.description;
			costReductionMod = currentLadleInUse.costReductionMod;
			purchaseCost = currentLadleInUse.purchaseCost;
		}

		// Update is called once per frame
		// As soon as the ladle has been upgraded, run method to switch SO to higher level
		// and update relevant variables to most recent values
		// Checks if needs to start courotine to reactivate button
		void Update()
		{
			if (ladleNeedsUpgrade)
			{
				SwitchLadle();
				description = currentLadleInUse.description;
				costReductionMod = currentLadleInUse.costReductionMod;
				purchaseCost = currentLadleInUse.purchaseCost;

				buttonText.text = currentLadleInUse.name;
				skillTipText.text = description + "[COST: " + purchaseCost + " ]";
				skillLevelText.text = skillLevel.ToString() + " / 5";
			}

			if (!ladleSkillButton.GetComponent<Button>().interactable && ladleButtonDisabled)
			{
				StartCoroutine(ReactivateButton(timer));
			}
		}

		// Overwrite the TimeCost in all ItemThroughHelper scripts attached to all spawned helpers
		// upon purchase of the upgrade. Sets bool to true, so method to swap SOs is triggered
		public void UpgradeLadles()
		{
			if (skillLevel < 5)
			{
				float checkCurrency = currencyCreation.Currency;
				checkCurrency -= purchaseCost;

				if (checkCurrency >= 0)
				{
					currencyCreation.Currency -= purchaseCost;
					activeHelpers = GameObject.FindGameObjectsWithTag(activeHelper);

					foreach (GameObject helper in activeHelpers)
					{
						helper.GetComponent<ItemThroughHelper>().TimeCost = baseTimeCost * costReductionMod;
					}

					skillLevel++;
					ladleNeedsUpgrade = true;
				}

				else
				{				
					ladleSkillButton.GetComponent<Button>().interactable = false;
					ladleButtonDisabled = true;
					timer = 5f;
					return;
				}				
			}

			else
			{
				//Displays in skillTip that max level is reached for this skill
				ladleSkillButton.GetComponent<Button>().interactable = false;
				string maxedOut = "Max ladle level is already reached!";
				skillTipText.text = maxedOut;
			}
		}

		// Method to switch the relevant SO depending on the skills current Level
		public void SwitchLadle()
		{
			for (int i = 0; i < ladleUpgrade.Length; i++)
			{
				switch (skillLevel)
				{
					case 0:
						currentLadleInUse = ladleUpgrade[0];
						break;
					case 1:
						currentLadleInUse = ladleUpgrade[1];
						break;
					case 2:
						currentLadleInUse = ladleUpgrade[2];
						break;
					case 3:
						currentLadleInUse = ladleUpgrade[3];
						break;
					case 4:
						currentLadleInUse = ladleUpgrade[4];
						break;
					case 5:
						currentLadleInUse = ladleUpgrade[5];
						break;
				}
			}

			ladleNeedsUpgrade = false;
		}

		// Coroutine to reactivate button with a delay
		public IEnumerator ReactivateButton(float timer)
		{
			ladleButtonDisabled = false;
			yield return new WaitForSeconds(timer);
			ladleSkillButton.GetComponent<Button>().interactable = true;		
		}
	}
}

