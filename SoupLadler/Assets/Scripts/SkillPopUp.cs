﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LAPMTech
{
	public class SkillPopUp : MonoBehaviour
	{
		[SerializeField]
		private GameObject skillPanel;
		private bool isPanelActive;
		private Button upgradesButton;

		// Gets relevant references and adds event to button
		private void Start()
		{
			upgradesButton = GetComponent<Button>();
			upgradesButton.onClick.AddListener(ManagePopUp);
		}
		
		// Method to activate/deactivate SkillMenu GO
		public void ManagePopUp()
		{
			if (!isPanelActive)
			{
				skillPanel.SetActive(true);
				isPanelActive = true;
			}

			else
			{
				skillPanel.SetActive(false);
				isPanelActive = false;
			}			
		}
	}
}

