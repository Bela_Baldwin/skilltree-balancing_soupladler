﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LAPMTech
{
	public class HelperManager : MonoBehaviour
	{
		[SerializeField]
		private HelperCreation helperGameObject;
		private ItemOnClick itemOnClick;

		[SerializeField]
		private HelperCreation[] helpers;

		public HelperCreation HelperGameObject
		{ get => helperGameObject; set => helperGameObject = value; }
		
		// Calls Method to select first helper once before the game is started
		void Awake()
		{
			itemOnClick = FindObjectOfType<ItemOnClick>();
			SwitchHelper();			
		}

		// Calls Method to update helper every frame
		void Update()
		{
			SwitchHelper();
		}

		// Method to switch the relevant SO depending on the overall item game score
		public void SwitchHelper()
		{
			for (int i = 0; i < helpers.Length; i++)
			{
				switch (itemOnClick.Item)
				{
					case 0:
						HelperGameObject = helpers[0];
						break;
					case 150:
						HelperGameObject = helpers[1];
						break;
					case 400:
						HelperGameObject = helpers[2];
						break;
					case 1000:
						HelperGameObject = helpers[3];
						break;
					case 3000:
						HelperGameObject = helpers[4];
						break;
				}
			}			
		}
	}
}

