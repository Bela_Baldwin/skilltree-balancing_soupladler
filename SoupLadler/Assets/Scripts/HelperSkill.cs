﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace LAPMTech
{
	public class HelperSkill : MonoBehaviour
	{
		[SerializeField]
		private HelperCreation localHelperGameObject;
		private string description;
		private float purchaseCost;
		private float rewardPerHelper;

		private CurrencyCreation currencyCreation;
		private ItemOnClick itemOnClick;

		[SerializeField]
		private HelperCreation [] helpers;
		private int helperCount;

		private GameObject helperSkillButton;
		private string helperButton = "HelperSkillButton";
		[SerializeField]
		private float timer;
		private bool helperButtonDisabled;

		[SerializeField]
		private RectTransform parent;

		[SerializeField]
		private GameObject helperPrefab;
		[SerializeField]
		private Vector2 [] spawnPosition;
		[SerializeField]
		private Queue<Vector2> spawnPositions = new Queue<Vector2>();
		
		[SerializeField]
		private TextMeshProUGUI buttonText;
		[SerializeField]
		private TextMeshProUGUI skillTipText;
		[SerializeField]
		private TextMeshProUGUI helperCountText;

		// Adds all predefined spawn positions to a queue
		private void Awake()
		{
			for (int i = 0; i < spawnPosition.Length; i++)
			{
				spawnPositions.Enqueue(spawnPosition[i]);
			}
		}

		// Start is called before the first frame update
		void Start()
		{
			helperSkillButton = GameObject.FindGameObjectWithTag(helperButton);
			currencyCreation = FindObjectOfType<CurrencyCreation>();
			itemOnClick = FindObjectOfType<ItemOnClick>();

			localHelperGameObject = FindObjectOfType<HelperManager>().HelperGameObject;
			timer = 0;
			helperCount = 0;
		}

		// Update is called once per frame
		// Checks if needs to start courotine to reactivate button
		// always keeps the button and skill info up to date
		void Update()
		{
			if (!helperSkillButton.GetComponent<Button>().interactable && helperButtonDisabled)
			{
				StartCoroutine(ReactivateButton(timer));
			}

			localHelperGameObject = FindObjectOfType<HelperManager>().HelperGameObject;

			buttonText.text = localHelperGameObject.name;
			skillTipText.text = localHelperGameObject.description + " [COST: "+localHelperGameObject.purchaseCost+" ]";
			helperCountText.text = helperCount.ToString() + " / 15";
		}

		// Method that checks if enough currency exists to activate skill and
		// if so spawns a new helper clone in the current spawn position for a certain cost
		public void SpawnHelper()
		{
			if (helperCount < 15)
			{
				purchaseCost = localHelperGameObject.purchaseCost;
				float checkCurrency = currencyCreation.Currency;
				checkCurrency -= purchaseCost;

				if (checkCurrency >= 0)
				{
					currencyCreation.Currency -= purchaseCost;
					Vector2 currentSpawnPosition = new Vector2();
					currentSpawnPosition = spawnPositions.Dequeue();
					Quaternion rotation = new Quaternion();

					helperPrefab.name = localHelperGameObject.name;
					helperPrefab.GetComponent<ItemThroughHelper>().LocalDescription = localHelperGameObject.description;
					helperPrefab.GetComponent<ItemThroughHelper>().LocalRewardPerHelper = localHelperGameObject.rewardPerHelper;

					GameObject helper = Instantiate(helperPrefab, parent.anchoredPosition, rotation, parent);
					helper.GetComponent<RectTransform>().anchoredPosition = currentSpawnPosition;
					helperCount++;
				}
				else
				{
					helperSkillButton.GetComponent<Button>().interactable = false;
					helperButtonDisabled = true;
					timer = 5f;
					return;
				}
			}

			else
			{
				//Displays in skillTip that max level is reached for this skill
				helperSkillButton.GetComponent<Button>().interactable = false;
				string maxedOut = "Max helper count is already reached!";
				skillTipText.text = maxedOut;
			}				
		}

		// Coroutine to reactivate button with a delay
		public IEnumerator ReactivateButton(float timer)
		{
			helperButtonDisabled = false;
			yield return new WaitForSeconds(timer);
			helperSkillButton.GetComponent<Button>().interactable = true;			
		}	
	}
}

