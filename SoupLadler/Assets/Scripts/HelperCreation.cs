﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LAPMTech
{
	[CreateAssetMenu(fileName = "New Helper", menuName = "ScriptableObjects/Helpers")]
	public class HelperCreation : ScriptableObject
	{
		public string name;
		public string description;

		public float purchaseCost;
		public float rewardPerHelper;
	}
}

