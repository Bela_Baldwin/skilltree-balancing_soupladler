﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

namespace LAPMTech
{
	public class CurrencyCreation : MonoBehaviour
	{
		private float currency;
		private float costReductionMod;
		private float rewardMod;
		private float currentItemCount;

		public float Currency
		{
			get { return currency; }
			set { currency = value; }
		}

		public float CostReductionMod
		{
			get { return costReductionMod; }
			set { costReductionMod = value; }
		}

		public float RewardMod
		{
			get { return rewardMod; }
			set { rewardMod = value; }
		}

		[SerializeField]
		private TextMeshProUGUI currencyDisplay;

		private ItemOnClick itemCount;

		private SoupTemperature soupTemperature;

		private float localPercentageMod;

		// Sets both the costReductionMod and rewardMod variables to the base value of 1 when starting the game
		// Gets the reference to the TMProUGUI attached to the GameObject
		// Gets the reference to the ItemOnClick and SoupTemperature script
		private void Start()
		{
			costReductionMod = 1;
			rewardMod = 1;
			currencyDisplay = GetComponent<TextMeshProUGUI>();
			itemCount = FindObjectOfType<ItemOnClick>();
			soupTemperature = FindObjectOfType<SoupTemperature>();
		}

		private void Update()
		{
			// Check if Soups have been increased since last currency calculation and
			// if so, reworks a mod value, then recalculates currency before displaying it
			if (currentItemCount != itemCount.Item)
			{
				ReworkPercentageMod();
				CalculateCurrency();
			}

			DisplayCurrency();
		}

		// A method do calculate the current Currency value
		public void CalculateCurrency()
		{
			// Local variable that saves the increment in the currency count
			float itemCountChange = new float();
			itemCountChange = itemCount.Item - currentItemCount;
			// Calculation of the current Currency value
			currency = currency + itemCountChange * rewardMod * localPercentageMod;
			// Saves current item count as a reference value
			currentItemCount = itemCount.Item;
			Debug.Log("newItemCount: " + currentItemCount);
			Debug.Log("Currency calculated with mod: " + rewardMod);
		}

		// A method do display the current Currency count as a score
		public void DisplayCurrency()
		{
			currencyDisplay.text = "Happiness: " + Math.Round(currency, 0).ToString();
		}

		// Reworks float value to represent multiplication with percentages
		private void ReworkPercentageMod()
		{
			localPercentageMod = soupTemperature.PercentageMod / 100;
		}
	}
}
