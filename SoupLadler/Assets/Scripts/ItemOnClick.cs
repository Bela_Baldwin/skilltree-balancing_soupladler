﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace LAPMTech
{
	public class ItemOnClick : MonoBehaviour
	{
		// The main variable of the game, the item (soup) that is created by clicking
		private float item;

		public float Item
		{
			get { return item; }
			set { item = value; }
		}
		[SerializeField]
		private TextMeshProUGUI scoreDisplay;
		private string gameScore = "txt_gameScore";

		// Sets the item count to base value of zero when starting the game
		// Gets the reference to the TMProUGUI of the scoreCounter GameObject
		void Start()
		{
			item = 0;
			scoreDisplay = GameObject.Find(gameScore).GetComponent<TextMeshProUGUI>();
		}

		// Runs the DisplayScore method every frame
		void Update()
		{
			DisplayScore();
		}

		// A method to count up the item count
		public void CreateItem()
		{
			item += 1;
		}

		// A method do display the current item count as a score
		public void DisplayScore()
		{
			scoreDisplay.text = "Hungry mouths fed: " + item.ToString();
		}
	}
}

