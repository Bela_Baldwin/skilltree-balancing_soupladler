﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LAPMTech
{
	public class ItemThroughHelper : MonoBehaviour
	{
		[SerializeField]
		private float localRewardPerHelper = 1f;
		private ItemOnClick itemOnClick;
		[SerializeField]
		private float timer;
		[SerializeField]
		private float timeCost = 10f;

		[SerializeField]
		private string localDescription;

		public float LocalRewardPerHelper
		{ get => localRewardPerHelper; set => localRewardPerHelper = value; }

		public string LocalDescription
		{ get => localDescription; set => localDescription = value; }

		public float TimeCost { get => timeCost; set => timeCost = value; }

		// Start is called before the first frame update
		// Gets relevant references
		void Start()
		{
			itemOnClick = FindObjectOfType<ItemOnClick>();
		}

		// Update is called once per frame
		void Update()
		{
			// Increases a timer
			timer += 1f * Time.deltaTime;
			// As soon as enough time has passed, creates a soup item
			// and resets timer to starting value
			if (timer >= timeCost)
			{
				timer = 0f;
				itemOnClick.Item += 1 * localRewardPerHelper;
				Debug.Log("Created item with "+localRewardPerHelper +" in" +timeCost);
			}
		}
	}
}

