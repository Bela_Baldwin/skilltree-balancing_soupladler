﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace LAPMTech
{
	public class BowlSkill : MonoBehaviour
	{
		[SerializeField]
		private BowlCreation [] bowlUpgrade;
		private string description;
		private float purchaseCost;
		private float rewardMod;
		private int skillLevel;
		[SerializeField]
		private BowlCreation currentBowlInUse;
		private bool bowlNeedsUpgrade;

		private CurrencyCreation currencyCreation;
		private GameObject bowlSkillButton;
		private string helperButton = "BowlSkillButton";
		[SerializeField]
		private float timer;
		private bool bowlButtonDisabled;

		[SerializeField]
		private TextMeshProUGUI buttonText;
		[SerializeField]
		private TextMeshProUGUI skillTipText;
		[SerializeField]
		private TextMeshProUGUI skillLevelText;

		// Updates relevant info for the button texts
		private void Awake()
		{
			description = currentBowlInUse.description;
			rewardMod = currentBowlInUse.rewardMod;
			purchaseCost = currentBowlInUse.purchaseCost;

			buttonText.text = currentBowlInUse.name;
			skillTipText.text = description + " [COST: " + purchaseCost + " ]";
			skillLevelText.text = skillLevel.ToString() + " / 5";
		}

		// Start is called before the first frame update
		// Update relevant variables to most recent values
		void Start()
		{
			description = currentBowlInUse.description;			
			purchaseCost = currentBowlInUse.purchaseCost;			
			rewardMod = currentBowlInUse.rewardMod;

			currencyCreation = FindObjectOfType<CurrencyCreation>();
			bowlSkillButton = GameObject.FindGameObjectWithTag(helperButton);
		}

		// Update is called once per frame
		// As soon as the bowl has been upgraded, run method to switch SO to higher level
		// and update relevant variables to most recent values
		// Checks if needs to start courotine to reactivate button
		void Update()
		{
			if (bowlNeedsUpgrade)
			{
				SwitchBowl();
				description = currentBowlInUse.description;
				rewardMod = currentBowlInUse.rewardMod;
				purchaseCost = currentBowlInUse.purchaseCost;

				buttonText.text = currentBowlInUse.name;
				skillTipText.text = description + "[COST: " + purchaseCost + " ]";
				skillLevelText.text = skillLevel.ToString() + " / 5";
			}

			if (!bowlSkillButton.GetComponent<Button>().interactable && bowlButtonDisabled)
			{
				StartCoroutine(ReactivateButton(timer));
			}
		}
				
		// Overwrite the reward mod of the currency creation formula
		// upon purchase of the upgrade. Sets bool to true, so method to swap SOs is triggered
		public void UpgradeBowls()
		{
			if (skillLevel < 5)
			{
				float checkCurrency = currencyCreation.Currency;
				checkCurrency -= purchaseCost;

				if (checkCurrency >= 0)
				{
					currencyCreation.Currency -= purchaseCost;
					currencyCreation.RewardMod = rewardMod;
					skillLevel++;
					bowlNeedsUpgrade = true;
				}

				else
				{
					bowlNeedsUpgrade = true;
					bowlSkillButton.GetComponent<Button>().interactable = false;
					bowlButtonDisabled = true;
					timer = 5f;
					return;				
				}
			}

			else
			{
				//Displays in skillTip that max level is reached for this skill
				bowlSkillButton.GetComponent<Button>().interactable = false;
				string maxedOut = "Max bowl level is already reached!";
				skillTipText.text = maxedOut;
			}
		}

		// Method to switch the relevant SO depending on the skills current Level
		public void SwitchBowl()
		{
			for (int i = 0; i < bowlUpgrade.Length; i++)
			{
				switch (skillLevel)
				{
					case 0:
						currentBowlInUse = bowlUpgrade[0];
						break;
					case 1:
						currentBowlInUse = bowlUpgrade[1];
						break;
					case 2:
						currentBowlInUse = bowlUpgrade[2];
						break;
					case 3:
						currentBowlInUse = bowlUpgrade[3];
						break;
					case 4:
						currentBowlInUse = bowlUpgrade[4];
						break;
					case 5:
						currentBowlInUse = bowlUpgrade[5];
						break;
				}
			}

			bowlNeedsUpgrade = false;
		}

		// Coroutine to reactivate button with a delay
		public IEnumerator ReactivateButton(float timer)
		{
			bowlButtonDisabled = false;
			yield return new WaitForSeconds(timer);
			bowlSkillButton.GetComponent<Button>().interactable = true;
		}
	}
}

