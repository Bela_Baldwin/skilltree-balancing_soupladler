﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LAPMTech
{
	[CreateAssetMenu(fileName = "New Bowl", menuName = "ScriptableObjects/Bowls")]
	public class BowlCreation : ScriptableObject
	{
		public string name;
		public string description;

		public float purchaseCost;
		public float rewardMod;
	}
}

