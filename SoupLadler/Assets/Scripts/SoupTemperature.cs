﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

namespace LAPMTech
{
	public class SoupTemperature : MonoBehaviour
	{
		[SerializeField]
		private float percentageMod;
		[SerializeField]
		private float reductionSpeed;

		[SerializeField]
		private TextMeshProUGUI temperatureDisplay;

		public float PercentageMod
		{ get => percentageMod; set => percentageMod = value; }
			   
		// Gets the reference to the TMProUGUI attached to the GameObject
		void Start()
		{
			temperatureDisplay = GetComponent<TextMeshProUGUI>();
		}

		// Runs methods to reduce and display the soup's temperature value
		void Update()
		{
			DisplayTemperature();
			ReductionOverTime();
		}

		// Display current soup temperature rounding its value to 2 decimal places
		public void DisplayTemperature()
		{
			temperatureDisplay.text = "Soup Temperature: " + Math.Round(percentageMod, 0) + "%";
		}

		// Reduce soup's temeprature by a small amount every frame
		public void ReductionOverTime()
		{
			percentageMod -= reductionSpeed * Time.deltaTime;
		}
	}
}
