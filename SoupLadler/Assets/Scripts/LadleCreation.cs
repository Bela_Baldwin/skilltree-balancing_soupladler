﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LAPMTech
{
	[CreateAssetMenu(fileName = "New Ladle", menuName = "ScriptableObjects/Ladles")]
	public class LadleCreation : ScriptableObject
	{
		public string name;
		public string description;

		public float purchaseCost;
		public float costReductionMod;
	}
}

