﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LAPMTech
{
	public class ButtonLogic : MonoBehaviour
	{
		private string menu = "MainMenu";
		private string game = "MaiNGame";

		// Method to load MainMenu scene
		public void LoadMainMenu()
		{
			SceneManager.LoadScene(menu);
		}
		// Method to load MainGame scene
		public void LoadMainGame()
		{
			SceneManager.LoadScene(game);
		}
		// Method to quit application
		public void ExitGame()
		{
			Application.Quit();
		}
	}
}

