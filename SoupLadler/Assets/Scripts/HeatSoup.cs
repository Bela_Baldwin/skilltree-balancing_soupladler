﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LAPMTech
{
	public class HeatSoup : MonoBehaviour
	{
		[SerializeField]
		private float cost;

		private CurrencyCreation currencyCreation;
		private SoupTemperature soupTemperature;

		private string button = "HeatSoupButton";
		[SerializeField]
		private GameObject heatSoupButton;
		private float timer;
		private bool heatSoupButtonDisabled;

		// Gets the reference to the CurrencyCreation, SoupTemperature script and the relevant button
		void Start()
		{
			currencyCreation = FindObjectOfType<CurrencyCreation>();
			soupTemperature = FindObjectOfType<SoupTemperature>();
			heatSoupButton = GameObject.FindGameObjectWithTag(button);			
		}

		// Checks if needs to start courotine to reactivate button
		private void Update()
		{
			if (!heatSoupButton.GetComponent<Button>().interactable && heatSoupButtonDisabled)
			{
				StartCoroutine(ReactivateButton(timer));
			}			
		}

		// Method that checks if enough currency exists to activate skill and
		// if so resets soup's temperature to full value for a certain cost
		public void HeatSoupForCurrency()
		{
			float checkCurrency = currencyCreation.Currency;
			checkCurrency -= cost;

			if (checkCurrency < 0)
			{
				heatSoupButton.GetComponent<Button>().interactable = false;
				heatSoupButtonDisabled = true;
				timer = 5f;
				return;
			}
			else
			{
				currencyCreation.Currency -= cost;
				soupTemperature.PercentageMod = 100f;
			}
		}

		// Coroutine to reactivate button with a delay
		public IEnumerator ReactivateButton(float timer)
		{
			heatSoupButtonDisabled = false;
			yield return new WaitForSeconds(timer);
			heatSoupButton.GetComponent<Button>().interactable = true;
		}
	}
}
